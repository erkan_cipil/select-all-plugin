
var targetValueAttribute = 'target-value';
var queryValueAttribute = 'target-query';
var visibleAllQuery = "#mainTable tr td.displayedCol input[type=checkbox]";
var readonlyAllQuery = "#mainTable tr td.readonlyCol input[type=checkbox]";
function checkTableAndInjectCheckbox(){
  //table id=mainTable which is container of field level security information
  var mainTable = document.getElementById('mainTable');
  //Check if the table is exist in DOM
  if(typeof(mainTable)!="undefined" && mainTable!=null){
    //Inspect for visible and readonly column header
    var visibleColumHeader =  document.querySelector("#mainTable tr th.displayedCol");
    var readOnlyColumHeader =  document.querySelector("#mainTable tr th.readonlyCol");
    /******* CREATE INPUT CHECKBOX FOR VISIBLE COLUMN********/
    var selectAllForVisibleColumn = document.createElement("input");
    selectAllForVisibleColumn.type="checkbox";
    selectAllForVisibleColumn.id="select_all_visible";
    selectAllForVisibleColumn.setAttribute(targetValueAttribute,true);
    selectAllForVisibleColumn.setAttribute(queryValueAttribute,visibleAllQuery);
    selectAllForVisibleColumn.addEventListener('change',selectAllOnClick);

    //APPEND ON TABLE HEADER
    visibleColumHeader.insertBefore(selectAllForVisibleColumn,visibleColumHeader.firstChild);

    /******* CREATE INPUT CHECKBOX FOR READONLY COLUMN********/
    var selectAllForReadOnlyColumn = document.createElement("input");
    selectAllForReadOnlyColumn.type="checkbox";
    selectAllForReadOnlyColumn.id="select_all_read_only";
    selectAllForReadOnlyColumn.setAttribute(targetValueAttribute,true);
    selectAllForReadOnlyColumn.setAttribute(queryValueAttribute,readonlyAllQuery);
    selectAllForReadOnlyColumn.addEventListener('change',selectAllOnClick);

    //APPEND ON TABLE HEADER
    readOnlyColumHeader.insertBefore(selectAllForReadOnlyColumn,readOnlyColumHeader.firstChild);
  }
}

function selectAllOnClick(e){
  //Select All element
  var targetElement = e.target;
  var targetValue =  targetElement.getAttribute(targetValueAttribute);
  var targetQuery = targetElement.getAttribute(queryValueAttribute);
  //Query for all input checkbox on column visible
  var dataCellForVisible =  document.querySelectorAll(targetQuery);
  for(k in dataCellForVisible){
    if(typeof(dataCellForVisible[k])=="object" && typeof(dataCellForVisible[k].checked)!="undefined"){
      dataCellForVisible[k].checked= targetValue=='true' ? true : false;
    }
  }

  //Toggle targetElement Value
  targetElement.setAttribute(targetValueAttribute,toggleVal(targetValue));
}
function toggleVal(val){
  if(val=='true')
  return 'false';
  else
  return 'true';
}
checkTableAndInjectCheckbox();

